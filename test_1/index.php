<?php
class Db
{
    public static function getConnection()
    {
        $dbhost = 'localhost';
        $dbname = 'evilcat9_agroiq';
        $dbuser = 'evilcat9_agroiq';
        $dbpassword = 'agroiq123456';

        $db = new PDO("mysql:host=".$dbhost.";dbname=".$dbname.";", $dbuser, $dbpassword);
        $db->exec("SET CHARACTER SET utf8");
        return $db;
    }
}

$db = Db::getConnection();

if (!isset($_COOKIE['visit'])) 
{
    $time = getdate();
    setcookie('visit', 1, time()+24*3600-(3600*$time['hours']+60*$time['minutes']+$time['seconds']));
    //echo date('d.m.Y H:i', time()+24*3600-(3600*$time['hours']+60*$time['minutes']+$time['seconds']));
    $db->query("UPDATE visits SET visit = visit+1");
}

$result = $db->query("SELECT visit FROM visits");
$result->setFetchMode(PDO::FETCH_ASSOC);
$row = $result->fetch();
echo "Количество уникальных пользователей: ".$row['visit'];


