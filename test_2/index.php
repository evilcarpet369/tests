<?php
class Db
{
    public static function getConnection()
    {
        $dbhost = 'localhost';
        $dbname = 'evilcat9_agroiq';
        $dbuser = 'evilcat9_agroiq';
        $dbpassword = 'agroiq123456';

        $db = new PDO("mysql:host=".$dbhost.";dbname=".$dbname.";", $dbuser, $dbpassword);
        $db->exec("SET CHARACTER SET utf8");
        return $db;
    }
}

$db = Db::getConnection();

$output = "";
$result = $db->query("SELECT (SELECT username FROM test_users WHERE id = user_id) as username, COUNT(user_id) AS purchases FROM test_orders GROUP BY user_id HAVING purchases >= 3");
$result->setFetchMode(PDO::FETCH_ASSOC);
while ($row = $result->fetch()) $output .= "<tr><td>".$row['username']."</td><td>".$row['purchases']."</td></tr>";

echo "<table border='1'><tr><th>пользователь</th><th>количество покупок</th></tr>".$output."</table>";


