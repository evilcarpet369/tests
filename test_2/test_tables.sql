-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Апр 08 2019 г., 16:46
-- Версия сервера: 5.7.21-20-beget-5.7.21-20-1-log
-- Версия PHP: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `evilcat9_agroiq`
--

-- --------------------------------------------------------

--
-- Структура таблицы `test_catalog`
--
-- Создание: Апр 08 2019 г., 13:37
-- Последнее обновление: Апр 08 2019 г., 13:41
--

DROP TABLE IF EXISTS `test_catalog`;
CREATE TABLE `test_catalog` (
  `id` int(11) NOT NULL,
  `name` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `test_catalog`
--

INSERT INTO `test_catalog` (`id`, `name`) VALUES
(1, 'товар 1'),
(2, 'товар 2'),
(3, 'товар 3'),
(4, 'товар 4'),
(5, 'товар 5'),
(6, 'товар 6'),
(7, 'товар 7'),
(8, 'товар 8');

-- --------------------------------------------------------

--
-- Структура таблицы `test_orders`
--
-- Создание: Апр 08 2019 г., 13:42
-- Последнее обновление: Апр 08 2019 г., 13:42
--

DROP TABLE IF EXISTS `test_orders`;
CREATE TABLE `test_orders` (
  `id` int(11) NOT NULL,
  `catalog_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `test_orders`
--

INSERT INTO `test_orders` (`id`, `catalog_id`, `user_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 4, 2),
(4, 5, 2),
(5, 6, 2),
(6, 8, 2),
(7, 4, 3),
(8, 5, 3),
(9, 5, 4),
(10, 1, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `test_users`
--
-- Создание: Апр 08 2019 г., 13:37
-- Последнее обновление: Апр 08 2019 г., 13:40
--

DROP TABLE IF EXISTS `test_users`;
CREATE TABLE `test_users` (
  `id` int(11) NOT NULL,
  `username` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `test_users`
--

INSERT INTO `test_users` (`id`, `username`) VALUES
(1, 'Пользователь 1'),
(2, 'Пользователь 2'),
(3, 'Пользователь 3'),
(4, 'Пользователь 4');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `test_catalog`
--
ALTER TABLE `test_catalog`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `test_orders`
--
ALTER TABLE `test_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `catalog_id` (`catalog_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `test_users`
--
ALTER TABLE `test_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `test_catalog`
--
ALTER TABLE `test_catalog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `test_orders`
--
ALTER TABLE `test_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `test_users`
--
ALTER TABLE `test_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
